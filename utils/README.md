## `waterfall.py`

Plot waterfall plots from CSV data.
Usage example:
```
./waterfall.py output.csv
```
